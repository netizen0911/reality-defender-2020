#**Reality Defender 2020**

##Live Site Pages:

1. [Landing Page ](http://rd.aifoundation.site/index.html)
2. [Request Access ](http://rd.aifoundation.site/request-acccess.html)
3. [Contribute As A Forensic Scientist ](http://rd.aifoundation.site/contribute.html)
4. [Support ](http://rd.aifoundation.site/donors.html)
5. [Upload Video or Link 1.0 ](http://rd.aifoundation.site/upload-video-link-1.html)
6. [Upload Video or Link 1.1 ](http://rd.aifoundation.site/upload-video-link-1.1.html)
7. [Scanning Video 1.0 ](http://rd.aifoundation.site/scanning-video-1.html)
8. [Scanning Video 1.1 ](http://rd.aifoundation.site/scanning-video-1.1.html)
9. [Upload Video or Link 2.0 ](http://rd.aifoundation.site/upload-video-link-2.html)
10. [ Upload Video or Link 2.1 ](http://rd.aifoundation.site/upload-video-link-2.1.html)
11. [ Upload Video 2.0 w/ Modal Requried ](http://rd.aifoundation.site/upload-video-alert.html)
12. [ Scanning Video 2.0 ](http://rd.aifoundation.site/scanning-video-2.html)
13. [ Scanning Video 2.1 ](http://rd.aifoundation.site/scanning-video-2.1.html)
14. [ Upload Video or Link 3.0 ](http://rd.aifoundation.site/upload-video-link-3.html)
15. [ Upload Video or Link 3.1 ](http://rd.aifoundation.site/upLoavideo-1-10.html)
16. [ Detail Report ](http://rd.aifoundation.site/detail-report.html)
17. [ FAQs ](http://rd.aifoundation.site/faq.html)